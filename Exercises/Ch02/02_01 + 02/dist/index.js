// const { createElement } = React
const { render } = ReactDOM

const style = {
    backgroundColor: 'orange',
    color: 'white',
    fontFamily: 'verdana'
}

// const title = createElement(
//     // What we want to create
//     'h1',
//     // The properties
//     {id: 'title', className: 'header', style: style},
//     // Any child content the element has
//     'Hello world!'
// )

render(
    // // Name of the element we want to render
    // title,
    // // Where to render on the DOM
    // document.getElementById('react-container')

    // JSX syntax
    <h1 id='title'
        className='header'
        style={style}>
        // Nested method
        // style = {{backgroundColor: 'orange', color: 'white', ...}}
        Hello world!
    </h1>,
    document.getElementById('react-container')
)
