# React
Intern: Nguyen Ngo Lap

## 1. What is React?

- A library for user interfaces
- Created at FB and Instagram
- React Native for mobile

\- React makes updating the DOM faster by using DOM Diffing

- Compares rendered content with the new UI changes
- Makes only the minimal changes necessary

## 2. Intro to JSX and Babel
### JSX
Javascript as XML - A tag-based syntax used to create React elements

### Webpack

- A module bundler
- Creates static files
- Automate processes

\- *import something = import the default export*

\- *import {something} = import the specified export*

## 3. React components
Syntax:

- `createClass` (React component syntax)
- ES6 class
- Stateless functional component: Takes in property info and return JSX

```
const MyComponent = (props) => (
        <div>{props.title}</div>
)
```
### React Icons
https://gorangajic.github.io/react-icons/

## 4. Props and state
\- Adding default props:

- createClass: Adding a new method
- ES6: Adding to a instance
- Stateless:
    - Same as ES6
    - Set default value for the properties

\- **PropTypes:** Supply a property type for the properties -> Can validate that the right type is supplied

- createClass: Adding new property to the class
- ES6: Adding to a class instance
- Stateless: Same as ES6
- Reference: https://www.npmjs.com/package/prop-types

### State
Represents the possible conditions of the applications:

- Editing/Saved
- Logged in/Logged out
- etc.

In React, we want to:

- Identify the minimal representation of app state
- Reduce state to as few components as possible
- Avoid overwriting state variables

\- *Note: Can't setup initial state for stateless functional component*

\- Reference:

- https://facebook.github.io/react-native/docs/state.html
- https://stackoverflow.com/questions/27991366/what-is-the-difference-between-state-and-props-in-react

## 6. Forms and Refs
\- **Refs:** Reaching out for individual elements to get their value

- Stateless components don't have `this` keyword -> Use callback function to get the value

## 7. React Lifecycle
Reference: http://busypeoples.github.io/post/react-component-lifecycle/